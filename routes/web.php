<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Crud generator
Route::get('/master/fetch', 'HomeController@fetch')->name('home');
Route::post('/master/submit', 'HomeController@submit')->name('home');
Route::patch('/master/modify', 'HomeController@modify')->name('home');
Route::delete('/master/remove', 'HomeController@delete')->name('home');
