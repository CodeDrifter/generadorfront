<?php $__env->startSection('content'); ?>
    <div class="content mx-5">
        <div class="columns">
            <div class="column is-one-third">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Master Form
                        </p>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <form action="" method="POST">
                                <b-field label="String">
                                    <b-input placeholder="String"></b-input>
                                </b-field>

                                <b-field label="Long String">
                                    <b-input type="textarea"
                                             minlength="10"
                                             maxlength="100"
                                             placeholder="Maxlength automatically counts characters">
                                    </b-input>
                                </b-field>

                                <b-field label="Numeric">
                                    <b-input placeholder="Number"
                                             type="number"
                                             min="0"
                                             max="20">
                                    </b-input>
                                </b-field>

                                <b-field label="Decimal">
                                    <b-input placeholder="Number"
                                             type="number"
                                             min="0"
                                             max="20">
                                    </b-input>
                                </b-field>

                                <b-field label="Date Picker">
                                    <b-datepicker
                                        placeholder="Type or select a date..."
                                        icon="calendar-today"
                                        editable>
                                    </b-datepicker>
                                </b-field>

                                <b-button type="is-primary">Submit</b-button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Master Table
                        </p>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <b-table :data="data">
                                <template slot-scope="props">
                                    <b-table-column field="id" label="ID" width="40" numeric>
                                        {{ props.row.id }}
                                    </b-table-column>

                                    <b-table-column field="masterString" label="Master String">
                                        {{ props.row.masterString }}
                                    </b-table-column>

                                    <b-table-column field="masterLongString" label="Master Long String">
                                        {{ props.row.masterLongString }}
                                    </b-table-column>

                                    <b-table-column field="masterNumeric" label="Master Numeric">
                                        {{ props.row.masterNumeric }}
                                    </b-table-column>

                                    <b-table-column field="masterDecimal" label="Master Decimal">
                                        {{ props.row.masterDecimal }}
                                    </b-table-column>

                                    <b-table-column field="masterDate" label="Master Date" centered>
                                        <span class="tag is-success">
                                            {{ props.row.masterDate }}
                                        </span>
                                    </b-table-column>

                                    <b-table-column label="Master Actions" width="150">
                                        <b-button type="is-primary" size="is-small">Update</b-button>
                                        <b-button type="is-danger" size="is-small">Delete</b-button>
                                    </b-table-column>
                                </template>
                            </b-table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('scripts'); ?>
    <script type="module">
        let mixLoginForm = {
            data() {
                return {
                    masterString: '',
                    masterLongString: '',
                    masterNumeric: '',
                    data: [
                        { 'id': 1, 'masterString': 'Jesse', 'masterLongString': 'Simmons', 'masterNumeric': 1, 'masterDecimal': 32.3, 'masterDate': '2016-10-15'},
                    ]
                }
            },
            computed: {
                validationString() {
                    return this.masterString.length > 0 && this.masterString.length < 255
                },
                validationLongString() {
                    return this.masterLongString.length > 0 && this.masterLongString.length < 110000000000000
                },
                validationNumeric() {
                    return this.masterNumeric.isNumber
                },
            },
            methods: {
                onSubmit(evt) {
                    evt.preventDefault();

                    if (this.validation) {
                        let uri = '/master/submit';

                        axios.post(uri, {
                            token: '<?php echo csrf_field(); ?>'
                        }).then(response => {
                            if (response.data) {
                                this.$toastr.s('Correcto', '¡Se ha subido!')
                            } else {
                                this.$toastr.e('Error', 'No se ha subido')
                            }
                        });
                    } else {
                        this.$toastr.e('Error', 'Llene el campo')
                    }

                },
                onModify(){
                    let uri = '/master/modify';

                    axios.patch(uri, {
                        token: '<?php echo csrf_field(); ?>'
                    }).then(response => {
                        if (response.data) {
                            this.$toastr.s('Correcto', '¡Se ha modificado!')
                        } else {
                            this.$toastr.e('Error', 'No se ha modificado')
                        }
                    });
                },
                onRemove(){
                    let uri = '/master/remove';

                    axios.delete(uri, {
                        token: '<?php echo csrf_field(); ?>'
                    }).then(response => {
                        if (response.data) {
                            this.$toastr.s('Correcto', '¡Se ha borrado!')
                        } else {
                            this.$toastr.e('Error', 'No se ha borrado')
                        }
                    });
                }
            }
        };

        window.pageMix.push(mixLoginForm);
    </script>

<?php $__env->stopPush(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\generador\resources\views/home.blade.php ENDPATH**/ ?>