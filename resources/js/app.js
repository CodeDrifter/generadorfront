// Boostrap y axios por defecto
require("./bootstrap");

window.Vue = require('vue');

// Modulos NPM
import VueToastr from "vue-toastr";
import Buefy from 'buefy'

// CSS para el Modulos
import 'buefy/dist/buefy.css'

// Importar a Vue
Vue.use(Buefy);
Vue.use(VueToastr);

// Buildear Vue
const app = new Vue({
    el: '#app',
    mixins: window.pageMix
});
