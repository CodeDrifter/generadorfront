@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <b-card
                    title="Filtros Master"
                    class="mb-2"
                >
                    @include('master.filter')
                </b-card>
            </div>
            <div class="col-md-9">
                <b-card
                    title="Tabla Master"
                    class="mb-2"
                >
                    @include('master.table')
                </b-card>
            </div>
        </div>
    </div>
@endsection

