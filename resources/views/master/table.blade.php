<b-table :items="items" :fields="fields" striped responsive="sm">
    <template v-slot:cell(acciones)="row">
        <b-button size="sm" @click="onModify" class="mr-2">
            Modificar
        </b-button>
        <b-button size="sm" @click="onRemove" class="mr-2" variant="danger">
            Borrar
        </b-button>
    </template>
</b-table>

@push('scripts')
    <script type="module">
        let mixDataTable = {
            data() {
                return {
                    fields: ['first_name', 'acciones'],
                    items: [
                        {first_name: 'Dickerson'},
                        {first_name: 'Larsen'}
                    ]
                }
            },
        };

        window.pageMix.push(mixDataTable);
    </script>

@endpush
