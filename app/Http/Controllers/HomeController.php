<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function fetch()
    {
        $obj = "true";
        return $obj;
    }

    public function submit()
    {
        $obj = "true";
        return $obj;
    }

    public function modify()
    {
        $obj = "true";
        return $obj;
    }

    public function delete()
    {
        $obj = "true";
        return $obj;
    }
}
